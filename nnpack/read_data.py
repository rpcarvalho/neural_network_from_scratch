from imageio import imread
import glob
import numpy as np


def load_mnist():
    """
    Load the MNIST png files from MNIST folder

    Returns:
        X_train, Y_train, X_test, Y_test

    """

    NUM_LABELS = 10
    # create list of image objects
    test_images = []
    test_labels = []

    for label in range(NUM_LABELS):
        for image_path in glob.glob("MNIST/Test/" + str(label) + "/*.png"):
            image = imread(image_path)
            test_images.append(image)
            letter = [0 for _ in range(0, NUM_LABELS)]
            letter[label] = 1
            test_labels.append(letter)

    # create list of image objects
    train_images = []
    train_labels = []

    for label in range(NUM_LABELS):
        for image_path in glob.glob("MNIST/Train/" + str(label) + "/*.png"):
            image = imread(image_path)
            train_images.append(image)
            letter = [0 for _ in range(0, NUM_LABELS)]
            letter[label] = 1
            train_labels.append(letter)

    X_train = np.array(train_images).reshape(-1, 784) / 255.0
    Y_train = np.array(train_labels)
    X_test = np.array(test_images).reshape(-1, 784) / 255.0
    Y_test = np.array(test_labels)

    return X_train, Y_train, X_test, Y_test
