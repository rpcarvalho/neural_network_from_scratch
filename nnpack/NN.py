import numpy as np


class NN:
    """
    Simple NN implementation for classification problems
    """

    def __init__(self):
        print(
            "Model Initialized\nCall .model() to define your NN and .fit(x,y) to start the training process\n"
        )
        pass

    ###################################
    #####          TOOLs         ######
    ###################################

    def onehot(self, arr):
        """
        Transoform input array into OneHotEncode format
        """
        a = np.array(arr)
        b = np.zeros((len(a), max(a) + 1))
        b[np.arange(len(a)), a] = 1
        return b

    def shuffle(self, a, b):
        """
        Shuffle a and b arrays together
        """
        c = np.c_[a.reshape(len(a), -1), b.reshape(len(b), -1)]
        a2 = c[:, : a.size // len(a)].reshape(a.shape)
        b2 = c[:, a.size // len(a) :].reshape(b.shape)
        np.random.shuffle(c)
        return a2, b2

    def model(self, units, activation):
        """
        Define the NN model details
        > units: tuple or list to specify dimensionality of W(l) weight matrices for each layer l
                 the last layer must have same dimension of n of classes
        > activation: tuple or list to specify activation function to be used in each output h(l)
        """
        self.start = 0
        self.layers = len(units)
        self.units = units
        self.a_type = ["linear"] + activation
        print("number of layers: ", self.layers)
        print("number of h-units", self.units)
        print("activation for each layer", activation)

    ###################################
    #####  ACTIVATION FUNCTIONS  ######
    ###################################

    def sigmoid(self, xx):
        """
        Compute the sigmoid of x
        """
        x = np.array(xx)
        s = lambda a: 1 / (1 + np.exp(-a))
        return s(x)

    def relu(self, x):
        """
        Compute the relu of x
        """
        xx = np.array(x)
        xx[xx < 0] = 0
        return xx

    def softmax(self, xx):
        """
        Compute the softmax
        """
        x = np.array(xx)
        nm = np.exp(x - np.max(x))
        sm = np.sum(nm, axis=1)
        temp = []
        for i in range(nm.shape[-1]):
            temp.append(nm[:, i] / sm)
        return np.array(temp).T

    def softmax_derivatives(self, x):
        s = np.reshape(x, (-1, 1))
        return np.diagflat(s) - np.dot(s, s.T)

    ###################################
    #######   LOSS FUNCTIONS   ########
    ###################################

    def cross_entropy(self, x, y, n):
        """
        Compute the cross entropy from a softmax output (x)
        y is the labels
        np should be in onehot format
        """
        loss = -1.0 * np.sum(np.multiply(y, np.log(x))) / n
        return loss

    def accuracy(self, y, n):
        """
        Compute accuracy
        """
        tot = 0.0
        for i in range(n):
            tot += 0.0 if np.argmax(self.h[-1][i]) == np.argmax(y[i]) else 1.0
        return 1.0 - tot / n

    ###################################
    #######    PROPAGATION     ########
    ###################################

    def activation(self, x, activation):
        if activation == "sigmoid":
            return self.sigmoid(x)
        elif activation == "relu":
            return self.relu(x)
        elif activation == "softmax":
            return self.softmax(x)
        elif activation == "linear":
            return np.array(x)
        else:
            print("Error with activation function specification")

    def act_derivative(self, xx, activation):
        x = np.array(xx)
        if activation == "sigmoid":
            """
            Compute the derivative of the sigmoid
            """
            t = self.sigmoid(x)
            ds = t * np.subtract(1.0, t)
            return ds

        elif activation == "relu":
            """
            Compute the derivative of the relu
            """
            ds = np.zeros_like(x)
            ds[np.where(x > 0)] = 1.0
            return ds

        elif activation == "linear":
            """
            Compute the derivative of linear activation
            """
            return np.ones_like(x)

        else:
            print("Error with activation function specification")

    def forward(self, x):
        """
        Process the forward propagation. Return the loss value and generate z=wx+b and h=actv(z)
        """
        # start forward process
        z = []  # z=wx+b
        h = []  # h=activation(z)
        z.append(x)
        h.append(x)
        for l in range(self.layers):
            z.append(
                np.matmul(self.w[l], h[l].reshape(len(h[l]), -1, 1)).reshape(
                    len(h[l]), -1
                )
                + self.b[l]
            )
            h.append(self.activation(z[l + 1], self.a_type[l + 1]))

        # global variables h,z to be used in backpropg
        self.h = h
        self.z = z
        return

    def backward2_obsolet(self, x, y):
        """
        Process the backpropagation and compute gradients
        """
        # start parameters
        final_grad = []  # gradients/w
        final_grad_b = []  # gradients/b
        phi = []  # recurrent terms in backpropagation

        ### LAST LAYER (L)
        # first term | derivative of Loss/softmax/output
        phi.append(np.subtract(self.h[-1], y))
        grad = []
        for i in range(self.n):
            a = self.h[-2][i].reshape(1, -1)
            b = phi[0][i].reshape(-1, 1)
            grad.append(np.matmul(b, a))
        final_grad.append(np.sum(grad, axis=0) / self.n)
        final_grad_b.append(np.sum(phi[0], axis=0) / self.n)

        if self.layers > 1:
            # loop for L-1 layers
            for l in range(1, self.layers):
                grad = []
                temp_phi = []
                d_actv = self.act_derivative(self.z[-l - 1], self.a_type[-l - 1])
                for i in range(self.n):
                    a = self.w[-l]
                    b = phi[l - 1][i]
                    c = np.matmul(a.T, b)
                    d = np.multiply(c, d_actv[i])
                    temp_phi.append(d)
                    d = d.reshape(-1, 1)
                    e = self.h[-l - 2][i].reshape(-1, 1)
                    grad.append(np.matmul(d, e.T))
                phi.append(temp_phi)
                final_grad.append(np.sum(grad, axis=0) / self.n)
                final_grad_b.append(np.sum(phi[l], axis=0) / self.n)

        temp = []
        temp2 = []
        for i in range(1, len(final_grad) + 1):
            temp.append(np.array(final_grad[-i]))
            temp2.append(np.array(final_grad_b[-i]))

        # final gradient in correct order
        grad_w = temp
        grad_b = temp2
        return grad_w, grad_b

    def backward(self, x, y):
        """
        Process the backpropagation and compute gradients
        """
        # start parameters
        final_grad = []  # gradients/w
        final_grad_b = []  # gradients/b
        phi = []  # recurrent terms in backpropagation

        ### LAST LAYER (L)
        # first term | derivative of Loss/softmax/output
        phi.append(np.subtract(self.h[-1], y))
        a = self.h[-2].reshape(self.n, 1, -1)
        b = phi[0].reshape(self.n, -1, 1)
        # for first layer
        grad = np.matmul(b, a)
        final_grad.append(np.sum(grad, axis=0) / self.n)
        final_grad_b.append(np.sum(phi[0], axis=0) / self.n)

        if self.layers > 1:
            # loop for L-1 layers
            for l in range(1, self.layers):
                d_actv = self.act_derivative(self.z[-l - 1], self.a_type[-l - 1])
                a = self.w[-l]
                b = phi[l - 1].reshape(self.n, -1, 1)
                c = np.matmul(a.T, b).reshape(self.n, -1)
                d = np.multiply(c, d_actv)
                phi.append(d)
                d = d.reshape(self.n, -1, 1)
                e = self.h[-l - 2].reshape(self.n, 1, -1)
                grad = np.matmul(d, e)
                final_grad.append(np.sum(grad, axis=0) / self.n)
                final_grad_b.append(np.sum(phi[l], axis=0) / self.n)

        temp = []
        temp2 = []
        for i in range(1, len(final_grad) + 1):
            temp.append(np.array(final_grad[-i]))
            temp2.append(np.array(final_grad_b[-i]))

        # final gradient in correct order
        grad_w = temp
        grad_b = temp2
        return grad_w, grad_b

    ###################################
    #######         FIT        ########
    ###################################

    def fit(
        self,
        x,
        y,
        epochs=100,
        lr=0.1,
        verbose=True,
        dc=True,
        test=None,
        n_batches=1,
        shuffle=True,
        init_rand=True,
    ):
        """
        Execute the training process, initializing and updating the network parameters
        At the end of fitting process, a dictionary will be returned with error and accuracy for each epoch

        >>>>> OPTIONS
        epochs: number of epochs to process the training
        n_batches: total number of batches (integer)
        shuffle: True/False to shuffle data at each epoch
        lr: learning rate. Float between 0,1
        dc: learning rate decay of type 1/x
        verbose: True/False to print results at each epoch
        test: list [X_test,Y_test] to validation
        """
        # initialize variables
        keep_x = np.array(x)
        keep_y = np.array(y)  # np.array(y)
        # history
        self.loss_history = []
        self.acc_history = []
        it_index = []
        ep_index = []

        # setting validation data
        if test is not None:
            # initialize test variables
            self.x_test = test[0]
            self.y_test = np.array(test[1])
            self.n_test = np.shape(self.y_test)[0]
            # test history
            self.loss_history_test = []
            self.acc_history_test = []

        ## initiating iterations
        for it in range(epochs):
            # print(lr)
            # shuffle the data at each iteration
            if shuffle == True:
                x, y = self.shuffle(keep_x, keep_y)
            else:
                x = keep_x
                y = keep_y

            # split the shuffled data at each iteration to get batches
            x = np.split(x, n_batches)
            y = np.split(y, n_batches)

            ## initiating batches
            cost = 0.0
            acc = 0.0
            for b_idx in range(n_batches):
                it_index.append(it * b_idx)
                self.x = x[b_idx]
                self.y = np.array(y[b_idx])
                self.n = np.shape(self.y)[0]

                ### STARTING WEIGHTS
                if self.start == 0:
                    # start parameters
                    w = []
                    b = []
                    if init_rand == False:
                        w.append(np.ones((self.units[0], self.x.shape[-1])))
                        b.append(np.ones((self.units[0])))
                        for l in range(self.layers - 1):
                            w.append(np.ones((self.units[l + 1], self.units[l])))
                            b.append(np.ones((self.units[l + 1])))
                    elif init_rand == "rand":
                        w.append(np.random.rand(self.units[0], self.x.shape[-1]))
                        b.append(np.random.rand(self.units[0]))
                        for l in range(self.layers - 1):
                            w.append(np.random.rand(self.units[l + 1], self.units[l]))
                            b.append(np.random.rand(self.units[l + 1]))
                    else:
                        w.append(
                            np.random.normal(
                                0, scale=0.01, size=(self.units[0], self.x.shape[-1])
                            )
                        )
                        b.append(np.random.normal(0, scale=0.01, size=(self.units[0])))
                        for l in range(self.layers - 1):
                            w.append(
                                np.random.normal(
                                    0,
                                    scale=0.01,
                                    size=(self.units[l + 1], self.units[l]),
                                )
                            )
                            b.append(
                                np.random.normal(
                                    0, scale=0.01, size=(self.units[l + 1])
                                )
                            )
                    self.w = w
                    self.b = b
                    self.start = 1
                ### END WEIGHTS

                # initial values
                self.forward(self.x)
                grad_w, grad_b = self.backward(self.x, self.y)

                ######################
                # starting optimization
                for i in range(len(grad_w)):
                    self.w[i] = self.w[i] - lr * grad_w[i]
                    self.b[i] = self.b[i] - lr * grad_b[i]

                # update cost and accuracy
                self.forward(self.x)
                cost = self.cross_entropy(self.h[-1], self.y, self.n)
                acc = self.accuracy(self.y, self.n)

                # acc=acc/n_batches
                # cost=cost/n_batches
                if test is not None:
                    if b_idx == n_batches - 1:
                        self.forward(self.x_test)
                        cost_test = self.cross_entropy(
                            self.h[-1], self.y_test, self.n_test
                        )
                        acc_test = self.accuracy(self.y_test, self.n_test)
                        print(
                            "%2d Batches, Epoch %2d -- Cost: %.3f Acc: %.3f  |-|  Test_Cost: %.3f | Acc: %.3f"
                            % (b_idx + 1, it + 1, cost, acc, cost_test, acc_test),
                            end="\r",
                        ) if verbose == True else None
                        self.loss_history.append(cost)
                        self.acc_history.append(acc)
                        self.loss_history_test.append(cost_test)
                        self.acc_history_test.append(acc_test)
                        ep_index.append(it * b_idx)
                    else:
                        print(
                            "%2d Batches, Epoch %2d -- Cost: %.3f Acc: %.3f"
                            % (b_idx + 1, it + 1, cost, acc),
                            end="\r",
                        ) if verbose == True else None
                        self.loss_history.append(cost)
                        self.acc_history.append(acc)
                else:
                    print(
                        "%2d Batches, Epoch %2d -- Cost: %.3f Acc: %.3f"
                        % (b_idx + 1, it + 1, cost, acc),
                        end="\r",
                    ) if verbose == True else None
                    self.loss_history.append(cost)
                    self.acc_history.append(acc)

            # jump one line
            print("")

            # decay
            lr = lr * (1 - 2 * lr / (it + 1)) if dc == True else lr

        if test is not None:
            dic = {
                "Iterations": it_index,
                "Epochs": ep_index,
                "Error": self.loss_history,
                "Accuracy": self.acc_history,
                "TestError": self.loss_history_test,
                "TestAccuracy": self.acc_history_test,
            }
        else:
            dic = {
                "Iterations": it_index,
                "Epochs": ep_index,
                "Error": self.loss_history,
                "Accuracy": self.acc_history,
            }
        return dic

    def get_w(self):
        """
        Get parameters after training
        """
        return self.w, self.b

    def evaluate(self, x, y):
        """
        Return the p[i] values for input data and print scores
        """
        # initialize variables
        n = len(x)
        self.forward(x)
        cost = self.cross_entropy(self.h[-1], y, n)
        acc = self.accuracy(y, n)
        # Print scores
        print("\nLoss: ", cost)
        print("Accuracy: ", acc, "\n")

        return self.h[-1]

    def predict(self, x):
        """
        Return the p[i] values for predictions
        """
        self.forward(x)
        return self.h[-1]
